local sprint_speed = 1.77
local gravity_rate = 1.17
-- Subjective, this value just felt good to me
local gravity_delay = 0.18

physics = {}
local players = {}

minetest.register_on_joinplayer(function(player)
    players[player:get_player_name()] = {}
    minetest.register_globalstep(function(dtime)
        -- Jump tweak
        if player:get_player_control().jump then
            minetest.after(gravity_delay, function()
                player:set_physics_override({
                    gravity = gravity_rate,
                    jump = 1 + (gravity_rate * gravity_delay)
                })
            end)
        end

        -- Sprinting
        if player:get_player_control().aux1 and player:get_player_control().up then
            player:set_physics_override({
                speed = sprint_speed
            })

            if players[player:get_player_name()] ~= nil then
                players[player:get_player_name()].sprinting = true
            end
        else
            player:set_physics_override({
                speed = 1.0
            })

            if players[player:get_player_name()] ~= nil then
                players[player:get_player_name()].sprinting = false
            end
        end
    end)
end)

function physics.is_sprinting(player)
    return players[player:get_player_name()].sprinting
end
